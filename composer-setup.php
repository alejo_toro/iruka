<?php

/*
 * This file is part of Composer.
 *
 * (c) Nils Adermann <naderman@naderman.de>
 *     Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

process(is_array($argv) ? $argv : array());

/**
 * processes the installer
 */
function process($argv)
{
    $check      = in_array('--check', $argv);
    $help       = in_array('--help', $argv);
    $force      = in_array('--force', $argv);
    $quiet      = in_array('--quiet', $argv);
    $channel    = in_array('--snapshot', $argv) ? 'snapshot' : (in_array('--preview', $argv) ? 'preview' : 'stable');
    $disableTls = in_array('--disable-tls', $argv);
    $installDir = false;
    $version    = false;
    $filename   = 'composer.phar';
    $cafile     = false;

    // --no-ansi wins over --ansi
    if (in_array('--no-ansi', $argv)) {
        define('USE_ANSI', false);
    } elseif (in_array('--ansi', $argv)) {
        define('USE_ANSI', true);
    } else {
        // On Windows, default to no ANSI, except in ANSICON and ConEmu.
        // Everywhere else, default to ANSI if stdout is a terminal.
        define('USE_ANSI',
            (DIRECTORY_SEPARATOR == '\\')
                ? (false !== getenv('ANSICON') || 'ON' === getenv('ConEmuANSI'))
                : (function_exists('posix_isatty') && posix_isatty(1))
        );
    }

    foreach ($argv as $key => $val) {
        if (0 === strpos($val, '--install-dir')) {
            if (13 === strlen($val) && isset($argv[$key+1])) {
                $installDir = trim($argv[$key+1]);
            } else {
                $installDir = trim(substr($val, 14));
            }
        }

        if (0 === strpos($val, '--version')) {
            if (9 === strlen($val) && isset($argv[$key+1])) {
                $version = trim($argv[$key+1]);
            } else {
                $version = trim(substr($val, 10));
            }
        }

        if (0 === strpos($val, '--filename')) {
            if (10 === strlen($val) && isset($argv[$key+1])) {
                $filename = trim($argv[$key+1]);
            } else {
                $filename = trim(substr($val, 11));
            }
        }

        if (0 === strpos($val, '--cafile')) {
            if (8 === strlen($val) && isset($argv[$key+1])) {
                $cafile = trim($argv[$key+1]);
            } else {
                $cafile = trim(substr($val, 9));
            }
        }
    }

    if ($help) {
        displayHelp();
        exit(0);
    }

    $ok = checkPlatform($quiet, $disableTls);

    if (false !== $installDir && !is_dir($installDir)) {
        out("The defined install dir ({$installDir}) does not exist.", 'info');
        $ok = false;
    }

    if (false !== $version && 1 !== preg_match('/^\d+\.\d+\.\d+(\-(alpha|beta)\d+)*$/', $version)) {
        out("The defined install version ({$version}) does not match release pattern.", 'info');
        $ok = false;
    }

    if (false !== $cafile && (!file_exists($cafile) || !is_readable($cafile))) {
        out("The defined Certificate Authority (CA) cert file ({$cafile}) does not exist or is not readable.", 'info');
        $ok = false;
    }

    if (true === $disableTls) {
        out("You have instructed the Installer not to enforce SSL/TLS security on remote HTTPS requests.", 'info');
        out("This will leave all downloads during installation vulnerable to Man-In-The-Middle (MITM) attacks.", 'info');
    }

    if ($check) {
        exit($ok ? 0 : 1);
    }

    if ($ok || $force) {
        installComposer($version, $installDir, $filename, $quiet, $disableTls, $cafile, $channel);
        exit(0);
    }

    exit(1);
}

/**
 * displays the help
 */
function displayHelp()
{
    echo <<<EOF
Composer Installer
------------------
Options
--help               this help
--check              for checking environment only
--force              forces the installation
--ansi               force ANSI color output
--no-ansi            disable ANSI color output
--quiet              do not output unimportant messages
--install-dir="..."  accepts a target installation directory
--preview            install the latest version from the preview (alpha/beta/rc) channel instead of stable
--snapshot           install the latest version from the snapshot (dev builds) channel instead of stable
--version="..."      accepts a specific version to install instead of the latest
--filename="..."     accepts a target filename (default: composer.phar)
--disable-tls        disable SSL/TLS security for file downloads
--cafile="..."       accepts a path to a Certificate Authority (CA) certificate file for SSL/TLS verification

EOF;
}

/**
 * check the platform for possible issues on running composer
 */
function checkPlatform($quiet, $disableTls)
{
    $errors = array();
    $warnings = array();

    $iniPath = php_ini_loaded_file();
    $displayIniMessage = false;
    if ($iniPath) {
        $iniMessage = PHP_EOL.PHP_EOL.'The php.ini used by your command-line PHP is: ' . $iniPath;
    } else {
        $iniMessage = PHP_EOL.PHP_EOL.'A php.ini file does not exist. You will have to create one.';
    }
    $iniMessage .= PHP_EOL.'If you can not modify the ini file, you can also run `php -d option=value` to modify ini values on the fly. You can use -d multiple times.';

    if (ini_get('detect_unicode')) {
        $errors['unicode'] = 'On';
    }

    if (extension_loaded('suhosin')) {
        $suhosin = ini_get('suhosin.executor.include.whitelist');
        $suhosinBlacklist = ini_get('suhosin.executor.include.blacklist');
        if (false === stripos($suhosin, 'phar') && (!$suhosinBlacklist || false !== stripos($suhosinBlacklist, 'phar'))) {
            $errors['suhosin'] = $suhosin;
        }
    }

    if (!function_exists('json_decode')) {
        $errors['json'] = true;
    }

    if (!extension_loaded('Phar')) {
        $errors['phar'] = true;
    }

    if (!extension_loaded('filter')) {
        $errors['filter'] = true;
    }

    if (!extension_loaded('hash')) {
        $errors['hash'] = true;
    }

    if (!extension_loaded('iconv') && !extension_loaded('mbstring')) {
        $errors['iconv_mbstring'] = true;
    }

    if (!ini_get('allow_url_fopen')) {
        $errors['allow_url_fopen'] = true;
    }

    if (extension_loaded('ionCube Loader') && ioncube_loader_iversion() < 40009) {
        $errors['ioncube'] = ioncube_loader_version();
    }

    if (version_compare(PHP_VERSION, '5.3.2', '<')) {
        $errors['php'] = PHP_VERSION;
    }

    if (version_compare(PHP_VERSION, '5.3.4', '<')) {
        $warnings['php'] = PHP_VERSION;
    }

    if (!extension_loaded('openssl') && true === $disableTls) {
        $warnings['openssl'] = true;
    } elseif (!extension_loaded('openssl')) {
        $errors['openssl'] = true;
    }

    if (extension_loaded('openssl') && OPENSSL_VERSION_NUMBER < 0x1000100f) {
        $warnings['openssl_version'] = true;
    }

    if (!defined('HHVM_VERSION') && !extension_loaded('apcu') && ini_get('apc.enable_cli')) {
        $warnings['apc_cli'] = true;
    }

    ob_start();
    phpinfo(INFO_GENERAL);
    $phpinfo = ob_get_clean();
    if (preg_match('{Configure Command(?: *</td><td class="v">| *=> *)(.*?)(?:</td>|$)}m', $phpinfo, $match)) {
        $configure = $match[1];

        if (false !== strpos($configure, '--enable-sigchild')) {
            $warnings['sigchild'] = true;
        }

        if (false !== strpos($configure, '--with-curlwrappers')) {
            $warnings['curlwrappers'] = true;
        }
    }

    if (!empty($errors)) {
        out("Some settings on your machine make Composer unable to work properly.", 'error');

        out('Make sure that you fix the issues listed below and run this script again:', 'error');
        foreach ($errors as $error => $current) {
            switch ($error) {
                case 'json':
                    $text = PHP_EOL."The json extension is missing.".PHP_EOL;
                    $text .= "Install it or recompile php without --disable-json";
                    break;

                case 'phar':
                    $text = PHP_EOL."The phar extension is missing.".PHP_EOL;
                    $text .= "Install it or recompile php without --disable-phar";
                    break;

                case 'filter':
                    $text = PHP_EOL."The filter extension is missing.".PHP_EOL;
                    $text .= "Install it or recompile php without --disable-filter";
                    break;

                case 'hash':
                    $text = PHP_EOL."The hash extension is missing.".PHP_EOL;
                    $text .= "Install it or recompile php without --disable-hash";
                    break;

                case 'iconv_mbstring':
                    $text = PHP_EOL."The iconv OR mbstring extension is required and both are missing.".PHP_EOL;
                    $text .= "Install either of them or recompile php without --disable-iconv";
                    break;

                case 'unicode':
                    $text = PHP_EOL."The detect_unicode setting must be disabled.".PHP_EOL;
                    $text .= "Add the following to the end of your `php.ini`:".PHP_EOL;
                    $text .= "    detect_unicode = Off";
                    $displayIniMessage = true;
                    break;

                case 'suhosin':
                    $text = PHP_EOL."The suhosin.executor.include.whitelist setting is incorrect.".PHP_EOL;
                    $text .= "Add the following to the end of your `php.ini` or suhosin.ini (Example path [for Debian]: /etc/php5/cli/conf.d/suhosin.ini):".PHP_EOL;
                    $text .= "    suhosin.executor.include.whitelist = phar ".$current;
                    $displayIniMessage = true;
                    break;

                case 'php':
                    $text = PHP_EOL."Your PHP ({$current}) is too old, you must upgrade to PHP 5.3.2 or higher.";
                    break;

                case 'allow_url_fopen':
                    $text = PHP_EOL."The allow_url_fopen setting is incorrect.".PHP_EOL;
                    $text .= "Add the following to the end of your `php.ini`:".PHP_EOL;
                    $text .= "    allow_url_fopen = On";
                    $displayIniMessage = true;
                    break;

                case 'ioncube':
                    $text = PHP_EOL."Your ionCube Loader extension ($current) is incompatible with Phar files.".PHP_EOL;
                    $text .= "Upgrade to ionCube 4.0.9 or higher or remove this line (path may be different) from your `php.ini` to disable it:".PHP_EOL;
                    $text .= "    zend_extension = /usr/lib/php5/20090626+lfs/ioncube_loader_lin_5.3.so";
                    $displayIniMessage = true;
                    break;

                case 'openssl':
                    $text = PHP_EOL."The openssl extension is missing, which means that secure HTTPS transfers are impossible.".PHP_EOL;
                    $text .= "If possible you should enable it or recompile php with --with-openssl";
                    break;
            }
            if ($displayIniMessage) {
                $text .= $iniMessage;
            }
            out($text, 'info');
        }

        out('');
        return false;
    }

    if (!empty($warnings)) {
        out("Some settings on your machine may cause stability issues with Composer.", 'error');

        out('If you encounter issues, try to change the following:', 'error');
        foreach ($warnings as $warning => $current) {
            switch ($warning) {
                case 'apc_cli':
                    $text = PHP_EOL."The apc.enable_cli setting is incorrect.".PHP_EOL;
                    $text .= "Add the following to the end of your `php.ini`:".PHP_EOL;
                    $text .= "    apc.enable_cli = Off";
                    $displayIniMessage = true;
                    break;

                case 'sigchild':
                    $text = PHP_EOL."PHP was compiled with --enable-sigchild which can cause issues on some platforms.".PHP_EOL;
                    $text .= "Recompile it without this flag if possible, see also:".PHP_EOL;
                    $text .= "    https://bugs.php.net/bug.php?id=22999";
                    break;

                case 'curlwrappers':
                    $text = PHP_EOL."PHP was compiled with --with-curlwrappers which will cause issues with HTTP authentication and GitHub.".PHP_EOL;
                    $text .= "Recompile it without this flag if possible";
                    break;

                case 'openssl':
                    $text = PHP_EOL."The openssl extension is missing, which means that secure HTTPS transfers are impossible.".PHP_EOL;
                    $text .= "If possible you should enable it or recompile php with --with-openssl";
                    break;

                case 'openssl_version':
                    // Attempt to parse version number out, fallback to whole string value.
                    $opensslVersion = trim(strstr(OPENSSL_VERSION_TEXT, ' '));
                    $opensslVersion = substr($opensslVersion, 0, strpos($opensslVersion, ' '));
                    $opensslVersion = $opensslVersion ? $opensslVersion : OPENSSL_VERSION_TEXT;

                    $text = PHP_EOL."The OpenSSL library ({$opensslVersion}) used by PHP does not support TLSv1.2 or TLSv1.1.".PHP_EOL;
                    $text .= "If possible you should upgrade OpenSSL to version 1.0.1 or above.";
                    break;

                case 'php':
                    $text = PHP_EOL."Your PHP ({$current}) is quite old, upgrading to PHP 5.3.4 or higher is recommended.".PHP_EOL;
                    $text .= "Composer works with 5.3.2+ for most people, but there might be edge case issues.";
                    break;
            }
            if ($displayIniMessage) {
                $text .= $iniMessage;
            }
            out($text, 'info');
        }

        out('');
        return true;
    }

    if (!$quiet) {
        out("All settings correct for using Composer", 'success');
    }
    return true;
}

/**
 * installs composer to the current working directory
 */
function installComposer($version, $installDir, $filename, $quiet, $disableTls, $cafile, $channel)
{
    $installPath = (is_dir($installDir) ? rtrim($installDir, '/').'/' : '') . $filename;
    $installDir  = realpath($installDir) ? realpath($installDir) : getcwd();
    $file        = $installDir.DIRECTORY_SEPARATOR.$filename;

    if (is_readable($file)) {
        @unlink($file);
    }

    $home = getHomeDir();
    file_put_contents($home.'/keys.dev.pub', <<<DEVPUBKEY
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAnBDHjZS6e0ZMoK3xTD7f
FNCzlXjX/Aie2dit8QXA03pSrOTbaMnxON3hUL47Lz3g1SC6YJEMVHr0zYq4elWi
i3ecFEgzLcj+pZM5X6qWu2Ozz4vWx3JYo1/a/HYdOuW9e3lwS8VtS0AVJA+U8X0A
hZnBmGpltHhO8hPKHgkJtkTUxCheTcbqn4wGHl8Z2SediDcPTLwqezWKUfrYzu1f
o/j3WFwFs6GtK4wdYtiXr+yspBZHO3y1udf8eFFGcb2V3EaLOrtfur6XQVizjOuk
8lw5zzse1Qp/klHqbDRsjSzJ6iL6F4aynBc6Euqt/8ccNAIz0rLjLhOraeyj4eNn
8iokwMKiXpcrQLTKH+RH1JCuOVxQ436bJwbSsp1VwiqftPQieN+tzqy+EiHJJmGf
TBAbWcncicCk9q2md+AmhNbvHO4PWbbz9TzC7HJb460jyWeuMEvw3gNIpEo2jYa9
pMV6cVqnSa+wOc0D7pC9a6bne0bvLcm3S+w6I5iDB3lZsb3A9UtRiSP7aGSo7D72
8tC8+cIgZcI7k9vjvOqH+d7sdOU2yPCnRY6wFh62/g8bDnUpr56nZN1G89GwM4d4
r/TU7BQQIzsZgAiqOGXvVklIgAMiV0iucgf3rNBLjjeNEwNSTTG9F0CtQ+7JLwaE
wSEuAuRm+pRqi8BRnQ/GKUcCAwEAAQ==
-----END PUBLIC KEY-----
DEVPUBKEY
);
    file_put_contents($home.'/keys.tags.pub', <<<TAGSPUBKEY
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0Vi/2K6apCVj76nCnCl2
MQUPdK+A9eqkYBacXo2wQBYmyVlXm2/n/ZsX6pCLYPQTHyr5jXbkQzBw8SKqPdlh
vA7NpbMeNCz7wP/AobvUXM8xQuXKbMDTY2uZ4O7sM+PfGbptKPBGLe8Z8d2sUnTO
bXtX6Lrj13wkRto7st/w/Yp33RHe9SlqkiiS4MsH1jBkcIkEHsRaveZzedUaxY0M
mba0uPhGUInpPzEHwrYqBBEtWvP97t2vtfx8I5qv28kh0Y6t+jnjL1Urid2iuQZf
noCMFIOu4vksK5HxJxxrN0GOmGmwVQjOOtxkwikNiotZGPR4KsVj8NnBrLX7oGuM
nQvGciiu+KoC2r3HDBrpDeBVdOWxDzT5R4iI0KoLzFh2pKqwbY+obNPS2bj+2dgJ
rV3V5Jjry42QOCBN3c88wU1PKftOLj2ECpewY6vnE478IipiEu7EAdK8Zwj2LmTr
RKQUSa9k7ggBkYZWAeO/2Ag0ey3g2bg7eqk+sHEq5ynIXd5lhv6tC5PBdHlWipDK
tl2IxiEnejnOmAzGVivE1YGduYBjN+mjxDVy8KGBrjnz1JPgAvgdwJ2dYw4Rsc/e
TzCFWGk/HM6a4f0IzBWbJ5ot0PIi4amk07IotBXDWwqDiQTwyuGCym5EqWQ2BD95
RGv89BPD+2DLnJysngsvVaUCAwEAAQ==
-----END PUBLIC KEY-----
TAGSPUBKEY
);

    if (false === $disableTls && empty($cafile) && !HttpClient::getSystemCaRootBundlePath()) {
        $errorHandler = new ErrorHandler();
        set_error_handler(array($errorHandler, 'handleError'));

        $target = $home . '/cacert.pem';
        $write = file_put_contents($target, HttpClient::getPackagedCaFile(), LOCK_EX);
        @chmod($target, 0644);

        restore_error_handler();

        if (!$write) {
            throw new RuntimeException('Unable to write bundled cacert.pem to: '.$target);
        }
        $cafile = $target;
    }

    $httpClient = new HttpClient($disableTls, $cafile);
    $uriScheme = false === $disableTls ? 'https' : 'http';

    if (!$version) {
        $versions = json_decode($httpClient->get($uriScheme . '://getcomposer.org/versions'), true);
        foreach ($versions[$channel] as $candidate) {
            if ($candidate['min-php'] <= PHP_VERSION_ID) {
                $version = $candidate['version'];
                $downloadUrl = $candidate['path'];
                break;
            }
        }

        if (!$version) {
            throw new RuntimeException('There is no version of Composer available for your PHP version ('.PHP_VERSION.')');
        }
    } else {
        $downloadUrl = "/download/{$version}/composer.phar";
    }

    $retries = 3;
    while ($retries--) {
        if (!$quiet) {
            out("Downloading $version...", 'info');
        }

        $url       = "{$uriScheme}://getcomposer.org{$downloadUrl}";
        $errorHandler = new ErrorHandler();
        set_error_handler(array($errorHandler, 'handleError'));

        // download signature file
        if (false === $disableTls) {
            $signature = $httpClient->get($url.'.sig');
            if (!$signature) {
                out('Download failed: '.$errorHandler->message, 'error');
            } else {
                $signature = json_decode($signature, true);
                $signature = base64_decode($signature['sha384']);
            }
        }

        $fh = fopen($file, 'w');
        if (!$fh) {
            out('Could not create file '.$file.': '.$errorHandler->message, 'error');
        }
        if (!fwrite($fh, $httpClient->get($url))) {
            out('Download failed: '.$errorHandler->message, 'error');
        }
        fclose($fh);

        restore_error_handler();
        if ($errorHandler->message) {
            continue;
        }

        try {
            // create a temp file ending in .phar since the Phar class only accepts that
            if ('.phar' !== substr($file, -5)) {
                copy($file, $file.'.tmp.phar');
                $pharFile = $file.'.tmp.phar';
            } else {
                $pharFile = $file;
            }

            // verify signature
            if (false === $disableTls) {
                $pubkeyid = openssl_pkey_get_public('file://'.$home.'/' . (preg_match('{^[0-9a-f]{40}$}', $version) ? 'keys.dev.pub' : 'keys.tags.pub'));
                $algo = defined('OPENSSL_ALGO_SHA384') ? OPENSSL_ALGO_SHA384 : 'SHA384';
                if (!in_array('SHA384', openssl_get_md_methods())) {
                    out('SHA384 is not supported by your openssl extension, could not verify the phar file integrity', 'error');
                    exit(1);
                }
                $verified = 1 === openssl_verify(file_get_contents($file), $signature, $pubkeyid, $algo);
                openssl_free_key($pubkeyid);
                if (!$verified) {
                    out('Signature mismatch, could not verify the phar file integrity', 'error');
                    exit(1);
                }
            }

            // test the phar validity
            if (!ini_get('phar.readonly')) {
                $phar = new Phar($pharFile);
                // free the variable to unlock the file
                unset($phar);
            }
            // clean up temp file if needed
            if ($file !== $pharFile) {
                unlink($pharFile);
            }
            break;
        } catch (Exception $e) {
            if (!$e instanceof UnexpectedValueException && !$e instanceof PharException) {
                throw $e;
            }
            // clean up temp file if needed
            if ($file !== $pharFile) {
                unlink($pharFile);
            }
            unlink($file);
            if ($retries) {
                if (!$quiet) {
                   out('The download is corrupt, retrying...', 'error');
                }
            } else {
                out('The download is corrupt ('.$e->getMessage().'), aborting.', 'error');
                exit(1);
            }
        }
    }

    if ($errorHandler->message) {
        out('The download failed repeatedly, aborting.', 'error');
        exit(1);
    }

    chmod($file, 0755);

    if (!$quiet) {
        out(PHP_EOL."Composer successfully installed to: " . $file, 'success', false);
        out(PHP_EOL."Use it: php $installPath", 'info');
    }
}

/**
 * colorize output
 */
function out($text, $color = null, $newLine = true)
{
    $styles = array(
        'success' => "\033[0;32m%s\033[0m",
        'error' => "\033[31;31m%s\033[0m",
        'info' => "\033[33;33m%s\033[0m"
    );

    $format = '%s';

    if (isset($styles[$color]) && USE_ANSI) {
        $format = $styles[$color];
    }

    if ($newLine) {
        $format .= PHP_EOL;
    }

    printf($format, $text);
}

function getHomeDir()
{
    $home = getenv('COMPOSER_HOME');
    if (!$home) {
        if (defined('PHP_WINDOWS_VERSION_MAJOR')) {
            if (!getenv('APPDATA')) {
                throw new RuntimeException('The APPDATA or COMPOSER_HOME environment variable must be set for composer to install correctly');
            }
            $home = strtr(getenv('APPDATA'), '\\', '/') . '/Composer';
        } else {
            if (!getenv('HOME')) {
                throw new RuntimeException('The HOME or COMPOSER_HOME environment variable must be set for composer to install correctly');
            }
            $home = rtrim(getenv('HOME'), '/') . '/.composer';
        }
    }

    if (!is_dir($home)) {
        @mkdir($home, 0777, true);
    }

    return $home;
}

function validateCaFile($contents)
{
    // assume the CA is valid if php is vulnerable to
    // https://www.sektioneins.de/advisories/advisory-012013-php-openssl_x509_parse-memory-corruption-vulnerability.html
    if (
        PHP_VERSION_ID <= 50327
        || (PHP_VERSION_ID >= 50400 && PHP_VERSION_ID < 50422)
        || (PHP_VERSION_ID >= 50500 && PHP_VERSION_ID < 50506)
    ) {
        return !empty($contents);
    }

    return (bool) openssl_x509_parse($contents);
}

class ErrorHandler
{
    public $message = '';

    public function handleError($code, $msg)
    {
        if ($this->message) {
            $this->message .= "\n";
        }
        $this->message .= preg_replace('{^copy\(.*?\): }', '', $msg);
    }
}

class HttpClient {

    private $options = array('http' => array());
    private $disableTls = false;

    public function __construct($disableTls = false, $cafile = false)
    {
        $this->disableTls = $disableTls;
        if ($this->disableTls === false) {
            if (!empty($cafile) && !is_dir($cafile)) {
                if (!is_readable($cafile) || !validateCaFile(file_get_contents($cafile))) {
                    throw new RuntimeException('The configured cafile (' .$cafile. ') was not valid or could not be read.');
                }
            }
            $options = $this->getTlsStreamContextDefaults($cafile);
            $this->options = array_replace_recursive($this->options, $options);
        }
    }

    public function get($url)
    {
        $context = $this->getStreamContext($url);
        $result = file_get_contents($url, null, $context);

        if ($result && extension_loaded('zlib')) {
            $decode = false;
            foreach ($http_response_header as $header) {
                if (preg_match('{^content-encoding: *gzip *$}i', $header)) {
                    $decode = true;
                    continue;
                } elseif (preg_match('{^HTTP/}i', $header)) {
    