<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

# Prod configuration
// ** MySQL settings - You can get this info from your web host ** //
$url = parse_url(getenv('DATABASE_URL') ? getenv('DATABASE_URL') : getenv('CLEARDB_DATABASE_URL'));

/** The name of the database for WordPress */
define('DB_NAME', trim($url['path'], '/'));

/** MySQL database username */
define('DB_USER', $url['user']);

/** MySQL database password */
define('DB_PASSWORD', $url['pass']);

/** MySQL hostname */
define('DB_HOST', $url['host']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
# -----------------------------------------------

# Local configuration
// // ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
// /** El nombre de tu base de datos de WordPress */
// define('DB_NAME', 'iruka');

// /** Tu nombre de usuario de MySQL */
// define('DB_USER', 'root');

// /** Tu contraseña de MySQL */
// define('DB_PASSWORD', 'root');

// /** Host de MySQL (es muy probable que no necesites cambiarlo) */
// define('DB_HOST', 'localhost');

// /** Codificación de caracteres para la base de datos. */
// define('DB_CHARSET', 'utf8mb4');

// /** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
// define('DB_COLLATE', '');
# --------------------------------------------

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
# Local configuration
// define('AUTH_KEY', '+g%z9aL< *78%I&*rhA=RheUeE^3B5yYixc{`?_5qiy~ |42`b887H KcR,h(,F0');
// define('SECURE_AUTH_KEY', 'e:$:X[A4CpmTzHWcn)$Y:/>IBSWQ{&_no{+oEVb2zFjzFovkfm^U#gj:+oo_)DnI');
// define('LOGGED_IN_KEY', 'V;W^He,k2st(a`e|4!VOrhqvS^H`A w=84J5[MKBz2af$|]eb!>X/sz<Y?7#C~J[');
// define('NONCE_KEY', '1J| *XPB,6/6{j(a;PD9TD|P2Bu-__&41<Cjg@+m>;8I#$kmU+@S>xl`,x`^3nR-');
// define('AUTH_SALT', '-<3V08}[u3H:OEgw*AT0wPlSjkwTc&!hgWZ<|%P=SHXP|f2<@r p&2C7L0AK{h)s');
// define('SECURE_AUTH_SALT', 'h889~hq )XqUHA^#_In;BhCZ9=JXyqIL$=Va=TxfTA^7#t;@!DEq:!{FN9!`d;~:');
// define('LOGGED_IN_SALT', 'htTpw7[@9.Vm^eyL?A.`K|}OeaS>.&i@u7,WY^cCMvl[Z;9t{4-,OJ0;AG%i<<cB');
// define('NONCE_SALT', 'F;E(dkA,:94.j87[H`;}|<UgJ1C,8,5/zs?RRo2[=9KdC)l`VVPZ95IehBctXIie');
# ----------------------------------------------
# Prod configuration
define('AUTH_KEY',         getenv('AUTH_KEY'));
define('SECURE_AUTH_KEY',  getenv('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY',    getenv('LOGGED_IN_KEY'));
define('NONCE_KEY',        getenv('NONCE_KEY'));
define('AUTH_SALT',        getenv('AUTH_SALT'));
define('SECURE_AUTH_SALT', getenv('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT',   getenv('LOGGED_IN_SALT'));
define('NONCE_SALT',       getenv('NONCE_SALT'));
# -----------------------------------------------
/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

